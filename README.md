# Python Speech to Typed Text

This is a very quick (and hacky) solution to render speech captured by a microphone on the local machine to text typed at the current cursor location. The cursor may be situated in any application (such as a browser or text editor).

The current implementation is configured for the Dutch language and can be prepared for English by commenting and uncommenting some lines. To add other languages, visit the Vosk site to get more speech models for the KaldiRecognizer (visit: https://alphacephei.com/vosk/models).

The speech recognition itself is processed offline using Vosk speech models. The quality of speech recognition results is influenced by the quality of the used models (among other things). The script contains some comments on how to add/change speech models.

## Some points of attention
The Vosk + KaldiRecognizer are very sensitive to noise and audio artefacts. If you have trouble with speech accuracy rates, it could be worthwhile to look into a digital (usb) noise-canceling fixed-distance microphone to improve performance.
