import pyautogui
from vosk import Model, KaldiRecognizer
from pyaudio import PyAudio, paInt16
import json
import os

# Vosk models for various languages can be found at: https://alphacephei.com/vosk/models
# This program assumes that the speech models are in a local directory relative to this script at ./Vosk models/

local_dir = os.path.dirname(__file__)
model_nl_location = os.path.join(local_dir,"Vosk models/vosk-model-small-nl-0.22")
#model_en_location = os.path.join(local_dir,"Vosk models/vosk-model-small-en-us-0.15")

sample_rate = 16000
chunk_size = 8000
# The chunk size determines how many samples are collected before analysis.
# If the sample rate is 16000 and the chunk_size 8000 that means that audio is processed every half second (16000 / 8000 = 2 (per second))
# Sample rate influences both required processing and accuracy while chunk_size influences responsiveness and required processing cycles

def load_speech_models():
    #if os.path.exists(model_en_location):
    #    global model_en
    #    model_en = Model(model_en_location)
    
    if os.path.exists(model_nl_location):
        global model_nl
        model_nl = Model(model_nl_location)
        global recognizer
        recognizer = KaldiRecognizer(model_nl, sample_rate)

def run_recognizer():
    if "recognizer" in globals():
        try:
            print("\nSpeech processing started.")

            stream = PyAudio().open(
                format=paInt16,
                channels=1,
                rate=sample_rate,
                input=True,
                frames_per_buffer=chunk_size
            )
            stream.start_stream()

            while True: 
                data = stream.read(chunk_size)
                if len(data) != 0:
                    if recognizer.AcceptWaveform(data):
                        result = json.loads(recognizer.Result())["text"]
                        if result != "":
                            # Any additional algorithms that should work on the captured text should go here
                            print(result)
                            pyautogui.write(result)
            
        except KeyboardInterrupt:
            print("\nSpeech processing stopped.")
    else:
        print("The speech processing model was not instantiated")


if __name__ == "__main__":
    load_speech_models()
    run_recognizer()
